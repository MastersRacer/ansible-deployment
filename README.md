# OtaFabLab

[portaali](https://otafablab.rip)

## Kokonaisuus

Infraan kuuluu
- yksi palvelin, esim hetzneristä
- N kpl tulostimia ja N kpl rippejä
  - ripeillä pyörii octoprint

Toiminnallisuudet
- portaali
  - portaalista pääsee octoprintteihin käsiksi hienon web ui:n avulla
  - portaalin lähdekoodi on kansiossa rip_portal
  - teknologiastäkki: Yew/WASM/Rust
- octoprint
  - octoprintillä voit tulostaa gkoodia
  - octoprinteissä ei ole authentikaatiota

Verkon rakenne
- palvelin kuuntelee wireguard vpn portissa 53/udp (koska espoon
  palomuuri)
- palvelimella pyörii portaali joka kuuntelee https
  - portaalia palvelee caddy
    - portaali on google oauthin ja caddy-auth-portaalin takana
    - caddy reverse proxyttää rpiN.fablab.rip yhteydet vpn sisälle
    - caddy vastaa cors kutsuista, octoprinteissä on cors pois päältä
- ripit yhdistävät palvelimen wireguard vpn verkkoon josta palvelin
  pystyy reverse proxyttämään trafiikkia niille

## Provisiointi

### Virtuaalipalvelimen provisiointi

Virtuaalipalvelimella pyörii wireguard ja caddy.

Caddyn voi provisioida komennolla

``` shell
ansible-playbook -vv playbook.yml --tags caddy --vault-id vault-id
```

> Tätä varten tarvitset `vault-id` salaisuuden, jolla voi dekryptata
> `inventory/group_vars/all/secret.yml`

🥔 Lisää tageja tulossa lähiaikoina 🥔

### Secret.Yml

Salaisuuksia voi muokata komennolla

``` shell
ansible-vault edit inventory/group_vars/all/secret.yml --vault-id vault-id
```

## Raspberry Pi provisiointi

- Lataa [Ubuntu Server LTS](https://ubuntu.com/download/raspberry-pi)
- ENNEN ENSIMÄÄISTA BOOTTIA muokkaa boot-osion /network-config -tiedostoa
  ```yaml
  version: 2
  ethernets:
    eth0:
      dhcp4: true
      optional: true
  wifis:
    wlan0:
      dhcp4: true
      optional: true
      access-points:
        espoo_oppilas:
          password: "Runeberg[salainen syntymävuosi]"
  ```

## TODO

- Proxy portaali
  - Tulostuksen prosentti liukuri
  - Abort nappula
  - Admin baneeli
- Ansiblella tarkista että octoprinteillä ei ole printtiä ajossa ennen
  kuin restarttaa
- YLÄPALKIN ASETUKSEEEET

# What does RIP mean?

`RIP` stands for `RIp-Portal`.
